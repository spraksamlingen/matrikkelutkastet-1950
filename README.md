# Matrikkelutkast 1950

Skapt: 02.01.2018 av Tarje Sælen Lavik

Matrikkelutkastet av 1950 inneholder 756525 matrikkeladresser, men inneholder ikke Oslo, Bergen og Finnmark (kanskje andre hull?). Regnearket er konvertert av Tarje, med hjelp av Peder Gammeltoft, i slutten av desember 2017.

TODO:
- [ ] Lage metadata på datasettet
- [ ] Spesifisesre named graph URI
- [ ] Tilgjengeliggjøre datasettet via et SPARQL Endpoint

Når dette er på plass kan det lages en indeks med ES. Data er i utgangspunktet flatt så å flate det ut i ES er enkelt. 
